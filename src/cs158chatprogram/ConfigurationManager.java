package cs158chatprogram;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Extracts the parameter settings from the CLI.
 *
 * The CLI parameters extracted as follows:
 * 
 * java ChatClient {local port number} {name} {remote port number}
 * 
 * - [0] local port number: Open local port number;
 * - [1] name: String identifier;
 * - [2] remote port number: Open port number on the remote.
 * 
 * @author Michael L.
 */
public final class ConfigurationManager {
	
	/**
	 * Returns the local and remote connections settings for the chat session.
	 * 
	 * @param params - CLI string parameters.
	 * @return Returns a ChatSessionConfiguration object.
	 * @throws NumberFormatException - Thrown when an illegal port number 
	 * is encountered.
	 * @throws UnknownHostException - Thrown for an unknown host.
	 */
	public static ChatSessionConfiguration getConfiguration(String[] params) 
	throws 
	NumberFormatException, 
	UnknownHostException 
	{
		return new ChatSessionConfiguration(
			new ChatConfiguration(
				InetAddress.getLocalHost(),
				Integer.parseInt(params[0]),
				params[1]),
			new ChatConfiguration(
				InetAddress.getLocalHost(),
				Integer.parseInt(params[2]),
				"REMOTE"));
	}
}
