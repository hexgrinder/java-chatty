package cs158chatprogram;

import java.util.concurrent.locks.ReentrantLock;

public class TerminalDisplay {
	
	private final static ReentrantLock lock_ = new ReentrantLock();
	
	public static void println(String string) {
		lock_.lock();
		System.out.println(string);
		lock_.unlock();
	}
	
	public static void print(String string) {
		lock_.lock();
		System.out.print(string);
		lock_.unlock();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		lock_.unlock();
		super.finalize();
	}
}
