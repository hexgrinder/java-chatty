package cs158chatprogram;

/**
 * Contains the local and remote connection pairing.
 * 
 * @author Michael L.
 */
public class ChatSessionConfiguration {
	
	public ChatSessionConfiguration(
		ChatConfiguration local, 
		ChatConfiguration remote) 
	{
		this.local = local;
		this.remote = remote;
	}
	
	public final ChatConfiguration local;
	public final ChatConfiguration remote;
}
