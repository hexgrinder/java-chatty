package cs158chatprogram;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * Implements the Runnable interface. 
 * 
 * Designed to run on a thread, a Carrier object will transmit the 
 * Message to the provided remote address.
 * 
 * @author Michael L.
 */
public class Carrier implements Runnable {

	private static final int DEFAULT_TIMEOUT = 30000;
	
	private final InetSocketAddress destination_;
	private Message message_;

	private Socket sock;
	
	public Carrier(InetSocketAddress destination, String message) {
		destination_ = destination;
		message_ = new Message(message);
	}
	
	@Override
	public void run() {
		try {
			
			sock = new Socket();
			sock.setSoTimeout(DEFAULT_TIMEOUT);
			
			try {
				sock.connect(destination_);
			} catch (IOException e) {
				TerminalDisplay.println(
					String.format("Cannot connect to %s", destination_));
				sock.close();
				return;
			}
			
			// write the message to socket output
			message_.write(sock.getOutputStream());
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			
			try {
				sock.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		
		super.finalize();
	
		if (null != sock && !sock.isClosed())
			sock.close();
		
	}
}

