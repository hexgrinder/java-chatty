package cs158chatprogram;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

/**
 * Entry point for the chat program. The program is initiated via
 * the CLI. The CLI parameters are as follows:
 * 
 * java ChatClient {local port number} {name} {remote port number}
 * 
 * - local port number: Open local port number;
 * - name: String identifier;
 * - remote port number: Open port number on the remote.
 * 
 * To quit the program, type ':q' at the prompt.
 * 
 * @author Michael L.
 */
public class ChatClient {

	private static ChatSessionConfiguration config_;
	
	public static void main(String[] args) {
		
		BufferedReader input = new BufferedReader(
			new InputStreamReader(System.in));
		
		ChatSession session = null;
		
		try {
			
			// extract configuration from CLI
			config_ = ConfigurationManager.getConfiguration(args);
			
			// setup destination address
			InetSocketAddress destaddr = new InetSocketAddress(
				config_.remote.ipAddress, 
				config_.remote.port);
				
			// start-up messaging session
			session = new ChatSession(config_.local);
			System.out.println(
				String.format("[INFO] Starting chat session on:\n%s", config_.local));
			session.start();
			
			
			// prompt line
			System.out.print("\nMessage [:q to quit]: ");
			
			String message;
						
			while (
				// detect for CTRL-C, or other break keystrokes
				null != (message = input.readLine())
				// detect for quit
				&& (0 != message.compareTo(":q"))) 
			{
				TerminalDisplay.println(
					String.format(
						"[%s]: %s",	config_.local.name, message));
				
				session.send(
					new Carrier(destaddr, message));	
				
				TerminalDisplay.print("\nMessage [:q to quit]: ");
			}
			
		} catch (NumberFormatException e) {
			TerminalDisplay.println("[ERROR] Invalid port number.");
		} catch (UnknownHostException e) {
			TerminalDisplay.println("[ERROR] Unknown host.");
		} catch (IOException e) {
			TerminalDisplay.println("[ERROR] Cannot read input line.");
		} finally {
			try {
				if (null != session) 
					session.stop();
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				TerminalDisplay.println("[INFO] Program exit.");
			}
		}
	}
}
