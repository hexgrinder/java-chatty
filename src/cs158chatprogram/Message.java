package cs158chatprogram;

import java.io.OutputStream;
import java.io.PrintWriter;

/**
 * Wraps a message for writing to an OutputStream.
 * 
 * @author Michael L.
 */
public class Message {
	
	private String message_;
	private PrintWriter pw_;

	public Message(String message) {
		message_ = message;
	}
	
	/**
	 * Writes the message to the provided OutputStream object.
	 * 
	 * @param stream - OutputStream object
	 */
	public void write(OutputStream stream) {
		pw_ = new PrintWriter(stream);
		pw_.write(message_);
		pw_.flush();
	}
	
	/**
	 * Closes the underlying writer, if any. 
	 */
	private void close() {
		if (null != pw_)
			pw_.close();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		close();
	}
}