package cs158chatprogram;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * This class is a chat service, sending messages and listening for
 * incoming messages from remotes. 
 *  
 * @author Michael L.
 */
public class ChatSession {
	
	/**
	 * Manages the listening server
	 */
	private ExecutorService inbound_ = Executors.newSingleThreadExecutor();
	
	/**
	 * Manages outbound messaging
	 */
	private ExecutorService outbound_ = Executors.newSingleThreadExecutor();
	
	private boolean running_ = false;
	private ServerSocket server_ = null;
	private Socket request_ = null;
	
	private final ChatConfiguration config_;
	
	/**
	 * Constructor assigns the connection settings that this 
	 * session will listen on.
	 * 
	 * @param config - Connection settings
	 */
	public ChatSession(ChatConfiguration config) {
		config_ = config;
	}
	
	/**
	 * Starts a non-blocking listen for incoming messages. Messages 
	 * received are output to the terminal.
	 * 
	 * Successive invocations of this method have no effect.
	 */
	public void start() {
		
		if (running_)
			return;
		
		running_ = true;
		
		inbound_.execute(new Runnable() {

			@Override
			public void run() {
				
				try {
					
					server_ = new ServerSocket(config_.port);
					
					while (running_) {
						
						// block call
						request_ = server_.accept();
						
						// NOT GOOD: better way than doing this...
						BufferedReader input = new BufferedReader(
							new InputStreamReader(request_.getInputStream()));
						
						TerminalDisplay.println(
							String.format("\n%45s",input.readLine().trim()));
					}
					
				} catch (IOException e) {
					if (null != server_ && !server_.isClosed())
						e.printStackTrace();
				} finally {
					try {
						if (null != server_)
							server_.close();
						if (null != request_)
							request_.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
			}
		});
	}

	/**
	 * Performs a non-blocking send. If the session has been stopped,
	 * the invocation has no effect and the message will not transmit.
	 *
	 * @param carrier - Message object.
	 */
	public void send(Carrier carrier) {
		if (!running_)
			return;
		
		outbound_.execute(carrier);
	}
	
	/**
	 * Stops the chat session. Closes down listening and
	 * message transmission. 
	 */
	public void stop() {
		
		inbound_.shutdownNow();
		outbound_.shutdownNow();
		
		try {
			
			if (null != server_ && !server_.isClosed())
				server_.close();
			
			if (null != request_ && !request_.isClosed())
				request_.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			running_ = false;
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#finalize()
	 */
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		stop();
	}
}
