package cs158chatprogram;

import java.net.InetAddress;

/**
 * Contains connection settings.
 * 
 * @author Michael L.
 */
public class ChatConfiguration {
	
	public ChatConfiguration(InetAddress address, int port, String name) {
		this.ipAddress = address;
		this.port = port;
		this.name = name;
	}
	
	/**
	 * Connection IP address. Read-only.
	 */
	public final InetAddress ipAddress;
	
	/**
	 * Connection port number. Read-only.
	 */
	public final int port;
	
	/**
	 * Name associated to this connection. Read=only.
	 */
	public final String name;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
			"Client ID: %s\nHost IP: %s\nPort: %d", 
			this.name,
			this.ipAddress,
			this.port);
	}
}
