# Description #

A toy chat-program built on the Java.net package. 

## Additional Documentation ##

Class documentation is included in the [doc folder](/doc).

# Example #

Figures 1 and 2 illustrate the chat sessions of Client 1 and Client 2, 
respectively. Messages from remote clients appear right-justified, 
whereas messages typed by the local client are left-justified and are 
prefixed with the client’s id.

![Figure 1](/img/figure1.PNG)
Figure 1: Chat Session on Client 1. Client 1 is on local host, port 6666.

![Figure 2](/img/figure2.PNG)
Figure 2: Chat Session on Client 2. Client 2 is on local host, port 7777.